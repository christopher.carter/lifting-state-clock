import React from 'react';
import Dashboard from './Components/Dashboard.js'
import './App.css';

import locations from './Constants'

function App() {
  return (
    <div className="App">
      <Dashboard 
        locations={locations}
      />
    </div>
  );
}

export default App;
