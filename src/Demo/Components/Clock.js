import React from 'react';
import moment from 'moment';
import timezone from 'moment-timezone'

class Clock extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      date: moment.tz(this.props.timezone).format('MMMM Do YYYY, h:mm:ss a')
    };
  }

  componentDidMount() {
    this.interval = setInterval(() => this.second(), 1000);
  }

  second() {
    this.setState({
      date: moment.tz(this.props.timezone).format('MMMM Do YYYY, h:mm:ss a')
    });
  }

  render() {
    return (
      <div>
        <div>
          <h1>{this.props.title}</h1>
          {this.state.date}
        </div>
      </div>
    );
  }
}

export default Clock