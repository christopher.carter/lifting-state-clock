import React from 'react';
import moment from 'moment';
import 'moment-timezone';

class Clock extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      clock: moment.tz(this.props.timezone).format('MMMM Do YYYY, h:mm:ss a'),
    };
  }

  componentDidMount() {
    this.interval = setInterval(() => this.second(), 1000);
  }

  second() {
    this.setState({
      clock: moment.tz(this.props.timezone).format('MMMM Do YYYY, h:mm:ss a')
    });
  }

  render() {
    return (
      <div>
        <div>
          <h1>{this.props.location}</h1>
          {this.state.clock}
        </div>
      </div>
    );
  }
}

export default Clock