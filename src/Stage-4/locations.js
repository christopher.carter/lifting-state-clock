export default [
  {
    location: "New York",
    timezone: "America/New_York"
  },
  {
    location: "Alaska",
    timezone: "America/Anchorage"
  },
  {
    location: "Dubai",
    timezone: "Asia/Dubai"
  }
]